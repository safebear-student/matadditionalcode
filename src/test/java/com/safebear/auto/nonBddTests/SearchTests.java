package com.safebear.auto.nonBddTests;

import com.safebear.auto.utils.Properties;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SearchTests extends BaseTest {

    @Test
    public void searchForItem(){
        // Setting test data in a single place. We can update this to pull data from a csv/data provider
        String toolName = "JMeter";

        // 1.Login as a valid user
        driver.get(Properties.getUrl());
        loginPage.login(userProvider.getUser("validUser"));
        Assert.assertEquals(toolsPage.getPageTitle(),toolsPage.getExpectedPageTitle());
        // 2. Search for JMeter (NOTE: you may want to add the tool then search for it, to ensure the data is present and correct)
        toolsPage.searchForTool(toolName);
        Assert.assertTrue(toolsPage.isElementInSearchResults(toolName));
    }
}
