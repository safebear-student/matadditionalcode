package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.ToolsPageLocators;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class ToolsPage {
    ToolsPageLocators locators = new ToolsPageLocators();
    @NonNull
    WebDriver driver;
    @Getter
    String expectedPageTitle = "Tools Page";

    public String getPageTitle() {
        return driver.getTitle();
    }

    public String getLoginSuccessMessage() {
        return driver.findElement(locators.getSuccessMessageLocator()).getText();
    }

    public void searchForTool(String searchTerm) {
        driver.findElement(locators.getSearchFieldLocator()).sendKeys(searchTerm);
        driver.findElement(locators.getSearchFieldLocator()).submit();
    }

    public boolean isElementInSearchResults(String searchTerm) {
        locators.setSearchResultElementLocator(By.xpath(".//td[.='" + searchTerm + "']"));
        return driver.findElements(locators.getSearchResultElementLocator()).size() != 0;
    }

}
