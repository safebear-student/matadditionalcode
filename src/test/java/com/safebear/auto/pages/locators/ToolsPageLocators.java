package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;

@Data
public class ToolsPageLocators {
    private String searchTerm;

    private By successMessageLocator = By.xpath("/html/body/div/p[2]/b");
    private By searchFieldLocator = By.xpath("//input[@id=\"toolName\"]");
    private By searchResultElementLocator = By.xpath(".//td[.='"+searchTerm+"']");
    private By jmeterResultElementLocator = By.xpath(".//td[.='JMeter']");

}
